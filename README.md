# Dokumentace k projektu EDIREX
* [Wiki](https://gitlab.ics.muni.cz/europdx/edirex-doc/wikis/home)
* [Issue Tracker](https://gitlab.ics.muni.cz/groups/europdx/-/issues)
* [Zapisy 2019](https://gitlab.ics.muni.cz/europdx/edirex-doc/wikis/Z%C3%A1pisy-ze-sch%C5%AFzek-2019)
* [Zapisy / Meeting minutes 2020](https://gitlab.ics.muni.cz/europdx/edirex-doc/-/wikis/Z%C3%A1pisy-ze-sch%C5%AFzek-2020)
* [Meeting minutes 2021](https://gitlab.ics.muni.cz/europdx/edirex-doc/-/wikis/Meeting-Minutes-2021/)

